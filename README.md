# Advanced Database project





## Prerequisites to run project

 
 To save time on cloning the project use this: https://github.com/cmderdev/cmder/releases/download/v1.3.5/cmder.zip terminal emulator. Just extract the zip file and you're good to go.

Copy this command to the terminal to get a fresh copy of the project:
 
 git clone https://gitlab.com/kcheverria9/advanced-database.git
 
 
 **Windows**

- Download and Install php first https://windows.php.net/downloads/releases/php-7.2.3-nts-Win32-VC15-x64.zip . It is recommended that you put this in your C folder as it will be much easier to locate. In this folder make a copy of php.ini-development file and rename it to php.ini as this would be your initiation file. Uncomment this line in the file' extension_dir = "ext" ' to enable php to pick up the extensions. Add these 2 lines to the end of the file in order to move to step 2.
     - extension=php_openssl.dll
     - extension=php_mbstring.dll

- Download and install composer https://getcomposer.org/Composer-Setup.exe . In the setup of composer it will request for your php terminal or something similar, so browse to where you extracted the php files and select php.exe. It would then proceed to install and composer would be installed.

- Download and install yarn and nodejs to be able to get packages to run the project. 
     - yarn can be downloaded from here https://yarnpkg.com/latest.msi
     - nodejs https://nodejs.org/dist/v8.10.0/node-v8.10.0-x64.msi

**On Linux(Ubuntu)**

- php is already installed. If not, run sudo apt install php7.2 php7.2-mbstring 

- To install composer just copy and paste these commands in the terminal https://getcomposer.org/download/

- To install yarn https://yarnpkg.com/lang/en/docs/install/

- To install Nodejs curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
sudo apt-get install -y nodejs

# Git FAQs
In order to make progress on the project, basic git commands are needed. 
- git pull: pulls the latest updates from the remote project
- git push: pushes your changes to a remote branch
- git checkout "branchname": moves your local branch to a branch that is remotely available 
- git checkout -b "branchname": Creates a new branch and moves to that branch
- git add: Stages your changes and allows you to make a commit(see next command)
git commit: Gives you a way to commit to your changes and then to be able to push them to git(No Changes can be pushed to while your local branch is on master so it would be advisable to create a new branch and move to that branch(with the name of the feature you will be working on)before making any changes). To know which branch you're on type the command git branch or if you're using an advanced text editor like Visual Studio Code just look in the lower left corner or an IDE like PhpStorm just look in the lower right corner to know what branch you are on. 
- To make a commit use git commit -m "Your commit message" Add WIP: to the beginning of your commit message if there's something wrong.

Any other problem that exists and i am not reachable, move on to a different feature and i will get back to you.

**Running the project** 

After cloning the project from git, cd into the directory that you cloned the project in and run the following commands

- cp .env.example .env and update .env to reflect your database set up(using sql server so use sqlsvr as DB_CONNECTION. 
- composer install: to install php packages 
- yarn install: to install npm packages
- yarn dev: to compile resources 
- php artisan serve: to run the project 
- localhost:8000 in the browser of your choice(Not internet explorer)



