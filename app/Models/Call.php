<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Call extends Model
{
    protected $table = 'Call';

    protected $primaryKey = 'Call_Number';

    function ticket(){
        return $this->hasOne(Ticket::class);
    }

}