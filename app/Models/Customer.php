<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'Customers';
    protected $primaryKey = 'ID';


    function creditCard(){
        return $this->hasMany(CreditCard::class);
    }

    function account(){
        return $this->hasMany(Account::class);
    }

    function getNameAttribute(){
        return "$this->First_Name $this->Last_Name";
    }

    function email(){
        return $this->hasMany(AccountHolderEmail::class);
    }

    function phoneNumber(){
        return $this->hasMany(AccountHolderPhone::class);
    }


}