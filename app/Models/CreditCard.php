<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model
{
protected $table = 'CreditCard';
protected $primaryKey = 'CardId';

protected $fillable = ['CardNum','CardType'];
public $timestamps = false;


function customer(){
    return $this->belongsTo(AccountHolder::class,'ID');
}

function payment(){
    return $this->hasMany(Payment::class,'CardNo');
}

}