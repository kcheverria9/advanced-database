<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'Account';
    protected $primaryKey = 'AccountNum';

    protected $fillable = ['AccountNum','Account_Type'];
    public $timestamps = false;

    function customer(){
        return $this->belongsTo(AccountHolder::class,'ID');
    }

}