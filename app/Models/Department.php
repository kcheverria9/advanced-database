<?php
/**
 * Created by PhpStorm.
 * User: khalin
 * Date: 3/12/18
 * Time: 12:19 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'Department';
    protected $primaryKey = 'Depart_id';

    function employee(){
        return $this->hasMany(Employee::class);
    }

    function supervisor(){
        return $this->employee()->where('Supervisor_id','=','null');
    }
}