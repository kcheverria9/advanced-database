<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'Payments';

    protected $primaryKey = 'PaymentNo';
    public $timestamps = false;

    protected $fillable = ['CardNo','payment_amount'];

    function creditCard(){
        return $this->belongsTo(CreditCard::class,'CardId');
    }

}