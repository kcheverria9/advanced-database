<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AccountHolder extends Model
{
 protected $table = 'AccountHolders';
 protected $primaryKey = 'ID';
public $timestamps = false;

 protected $fillable = ['First_Name','Last_Name','Gender','DOB','Address',
     'Salary','Depart_ID','Supervisor_id'];

 protected $hidden = ['DOB','Depart_ID','Supervisor_id'];


    function creditCard(){
        return $this->hasMany(CreditCard::class, 'ID');
    }

    function account(){
        return $this->hasMany(Account::class,'ID');
    }


 function email(){
     return $this->hasMany(AccountHolderEmail::class);
 }

 function phoneNumber(){
     return $this->hasMany(AccountHolderPhone::class);
 }

 function getNameAttribute(){
     return "$this->First_Name $this->Last_Name";
 }


}