<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AccountHolderPhone extends Model
{
protected $table = 'AccountHolders_PhoneNumber';

public $timestamps = false;

protected $fillable = ['PhoneNumber'];

function accountHolder(){
    return $this->belongsTo(AccountHolder::class);
}
}