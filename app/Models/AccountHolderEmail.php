<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AccountHolderEmail extends Model
{
    protected $table = 'AccountHolders_Email';

    public $timestamps =false;

    protected $fillable = ['Email'];

    function accountHolder(){
        return $this->belongsTo(AccountHolder::class, 'account_holder_id');
    }

    function customer(){
        return $this->belongsTo(Customer::class, 'account_holder_id');
    }

    function employee(){
        return $this->belongsTo(Employee::class);
    }

}