<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'Ticket';

    protected $primaryKey = 'Ticket_Number';

    protected $fillable = ['category','Date_of_call','ID'];

    public $timestamps = false;

    function call(){
        return $this->belongsTo(Call::class);
    }

    function employee(){
        return $this->hasOne(Employee::class, 'ID', 'Ticket_Number');
    }

}