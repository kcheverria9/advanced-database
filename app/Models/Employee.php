<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'Employees';
    protected $primaryKey = 'ID';

    function department(){
      return $this->belongsTo(Department::class, 'Depart_ID');
    }

    function ticket(){
        return $this->hasMany(Ticket::class, 'Ticket_Number','ID');
    }

    function getNameAttribute(){
        return "$this->First_Name $this->Last_Name";
    }

    function email(){
        return $this->hasMany(AccountHolderEmail::class);
    }

function phoneNumber(){
        return $this->hasMany(AccountHolderPhone::class);
    }


}