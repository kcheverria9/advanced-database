<?php

namespace App\Http\Controllers;

use App\Models\Call;
use App\Models\CreditCard;
use App\Models\Customer;
use App\Models\Employee;
use App\Models\Payment;
use App\Models\Ticket;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class HomeController extends Controller
{
    public function _construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $role = Auth::user()->role;
        switch ($role) {
            case 'administrator':
                return $this->admin();
                break;
            case 'supervisor':
                return $this->supervisor();
                break;
            case 'employee':
                return $this->employee();
                break;
            case 'customer':
                return $this->customer();
                break;
            default:
                return redirect('login');
        }
    }

    public function admin()
    {
        $calls = Call::all()->count();
        $transactions = Transaction::all()->count();
        $moneyMade = Payment::all()->sum('payment_amount');
        $dashboard = array($calls, $transactions, $moneyMade);
        return view('administrator.home', compact('dashboard'));
    }

    public function employee()
    {
        $employee = Employee::where('user_id', Auth::user()->id)->first();
        $tickets = Ticket::where('ID', $employee->ID)->get();
        return view('employee.home')->with(compact('tickets'));
    }

    public function supervisor()
    {
        $employee = Employee::where('user_id', Auth::user()->id)->first();
        $tickets = Ticket::where('ID', $employee->ID)->get();
        return view('employee.home')->with(compact('tickets'));

    }

    public function customer()
    {
        $customer = Customer::where('user_id', Auth::user()->id)->first();
        $payments = CreditCard::with(['Payment', 'Customer'])->where('ID', '=', $customer->ID)->get();
        return view('customer.home')->with(compact('payments'));
    }

}
