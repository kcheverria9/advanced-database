<?php

namespace App\Http\Controllers\Admin;

use App\Models\AccountHolder;
use App\Models\AccountHolderEmail;
use App\Models\AccountHolderPhone;
use App\Models\Call;
use App\Models\Department;
use App\Models\Employee;
use App\Models\Payment;
use App\Models\Role;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;


class AdminController extends Controller
{

    public function _construct()
    {
        $this->middleware('auth');
    }


    function employee()
    {
        $employees = Employee::all();
        return view('administrator.employees', compact('employees'));
    }

    function form($id = null)
    {
        $departments = Department::all();
        if ($id != null) {
            $employee = AccountHolder::where('ID', $id)->first();
            $address = explode(',', $employee->Address, 2);
            if ($employee->user_id != null) {
                $role = User::where('id', $employee->user_id)->first()->role;
            } else {
                $role = '';
            }
        } else {
            $employee = new AccountHolder();
            $role = '';
        }
        return view('administrator.employee')->with(compact('employee', 'departments', 'address', 'role'));
    }

    function saveEmployee(Request $request)
    {
        $validatedData = $request->validate([
            'First_Name' => 'required',
            'Last_Name' => 'required',
            'email' => 'required|email',
            'DOB' => 'required',
            'Gender' => 'required',
            'salary' => 'required|numeric',
            'Department' => 'required',
            'phone_number' => 'required|max:14',
            'street_name' => 'required',
            'parish' => 'required'
        ]);

        if ($request->get('ID') == '') {
            $user = new User();
            $password = bcrypt('Pa$$w0rd123');
            $user->name = $request->get('First_Name') . ' ' . $request->get('Last_Name');
            $user->email = $request->get('email');
            $user->password = $password;
            $user->save();
            $employee = new AccountHolder($request->all());
            $employee->Address = $request->get('street_name') . ', ' . $request->get('parish');
            $employee->user_id = $user->id;
            $employee->Salary = $request->get('salary');
            $employee->Depart_ID = $request->get('Department');
            $employee->Supervisor_id =
                AccountHolder::where('Supervisor_id', null)
                    ->where('Depart_ID', $request->get('Department'))
                    ->pluck('ID')->first();

            if ($request->get('role') == 'supervisor') {
                $user->roles()->attach(Role::where('name', 'supervisor')->first());
            } else {
                $user->roles()->attach(Role::where('name', 'employee')->first());
            }


            $employee->save();

            $employee->email()->save(new AccountHolderEmail(['Email' => $request->get('email')]));
            $employee->phoneNumber()->save(new AccountHolderPhone(['PhoneNumber' => $request->get('phone_number')]));
        } else {
            $employee = AccountHolder::find($request->get('ID'));
            if ($employee->user_id != null) {
                $user = User::where('id', $employee->user_id)->first();
                $user->name = $request->get('First_Name') . ' ' . $request->get('Last_Name');
                $user->save();

                $employee->Address = $request->get('street_name') . ', ' . $request->get('parish');

                $employee->user_id = $user->id;
                $employee->Salary = $request->get('salary');
                $employee->Depart_ID = $request->get('Department');
                $employee->Supervisor_id =
                    AccountHolder::where('Supervisor_id', null)
                        ->where('Depart_ID', $request->get('Department'))
                        ->pluck('ID')->first();

                $employee->push();
            }

        }
        return redirect('employees');
    }
}
