<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Request;
use App\Models\Ticket;
use App\Models\Employee;


class TicketController extends Controller
{
    public function view_ticket_between_period($startDate, $endDate){
        //echo $startDate.'</br>';
        //echo $endDate;
        $temp = Ticket::whereBetween('Date_of_Call', array($startDate, $endDate))->get();
        //dd($temp);
        return view('tickets.ticketOverPeriod', ['Ticket' => $temp]);
    }

    public function view_ticket_by_employee_over_period($empId, $startDate, $endDate){
        $getEmpId = Employee::find($empId);
        $tickets = Ticket::whereBetween('Date_of_Call', array($startDate, $endDate))->where('ID', $getEmpId->ID);
       dd( $tickets);
    }

}
