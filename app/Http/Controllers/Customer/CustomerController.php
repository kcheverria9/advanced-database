<?php

namespace App\Http\Controllers\Customer;

use App\Models\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CustomerController extends Controller
{
    function payment(Request $request){

        $validate = $request->validate(['payment_amount']);

        $payment = new Payment($request->all());
        $payment->Date = \Carbon\Carbon::now()->toDateTimeString();
        $payment->save();
        return redirect('home');
    }

}
