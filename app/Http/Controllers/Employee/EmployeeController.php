<?php

namespace App\Http\Controllers\Employee;

use App\Models\Account;
use App\Models\AccountHolder;
use App\Models\AccountHolderEmail;
use App\Models\AccountHolderPhone;
use App\Models\CreditCard;
use App\Models\Department;
use App\Models\Employee;
use App\Models\Role;
use App\Models\Ticket;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class EmployeeController extends Controller
{


    function employees()
    {
        $employee = AccountHolder::where('user_id', Auth::user()->id)->first();

        $departmentName = Department::where('Depart_id', $employee->Depart_ID)->pluck('Department_Name')->first();
        $employees = DB::select('exec sp_get_employees_in_department ?', array($departmentName));
        $employeeArray = Employee::hydrate($employees);
        return view('employee.employees')->with(compact('employeeArray'));
    }


    function form($id = null)
    {
        if ($id != null) {
            $customer = AccountHolder::where('ID', $id)->first();
            $address = explode(',', $customer->Address, 2);
            $card = CreditCard::where('ID', $customer->ID)->first();
            $account = Account::where('ID', $customer->ID)->first();
        } else {
            $customer = new AccountHolder();
            $card = new CreditCard();
            $account = new Account();
        }
        return view('employee.form')->with(compact('customer', 'address', 'card', 'account'));
    }

    function saveCustomer(Request $request)
    {
        $validatedData = $request->validate([
            'First_Name' => 'required',
            'Last_Name' => 'required',
            'email' => 'required|email',
            'DOB' => 'required',
            'Gender' => 'required',
            'phone_number' => 'required|max:14',
            'street_name' => 'required',
            'parish' => 'required',
            'account_number' => 'required',
            'AccountType' => 'required',
            'card_number' => 'required',
            'CardType' => 'required'
        ]);

        $existingEmployee = User::where('email', $request->email)->first();
        if ($request->get('ID') == '' && !($existingEmployee)) {
            $user = new User();
            $password = bcrypt('Pa$$w0rd123');
            $user->name = $request->get('First_Name') . ' ' . $request->get('Last_Name');
            $user->email = $request->get('email');
            $user->password = $password;
            $user->save();

            $user->roles()->attach(Role::where('name', 'customer')->first());

            $customer = new AccountHolder($request->all());
            $customer->Address = $request->get('street_name') . ', ' . $request->get('parish');
            $customer->user_id = $user->id;
            $customer->save();
            $customer->email()->save(new AccountHolderEmail(['Email' => $request->get('email')]));
            $customer->phoneNumber()->save(new AccountHolderPhone(['PhoneNumber' => $request->get('phone_number')]));

            $customer->creditCard()->save(new CreditCard([
                'CardNum' => str_replace('-', '', $request->get('card_number')),
                'CardType' => $request->get('CardType')
            ]));
            $customer->account()->save(new Account(
                ['AccountNum' => str_replace(['-'], '', $request->get('account_number')),
                    'Account_Type' => $request->get('AccountType')]));

        } else {
            $customer = AccountHolder::find($request->get('ID'));
            if ($customer->user_id != null) {
                $user = User::where('id', $customer->user_id)->first();
                $user->name = $request->get('First_Name') . ' ' . $request->get('Last_Name');
                $user->save();
                $customer->Address = $request->get('street_name') . ', ' . $request->get('parish');
                $customer->user_id = $user->id;

                if ($customer->Salary != null) {
                    $customer->creditCard->CardNum = $request->get('cardNumber');
                    $customer->creditCard->CardType = $request->get('cardType');
                    $customer->account->AccountNum = $request->get('accountNumber');
                    $customer->account->Account_Type = $request->get('accountType');
                }
                $customer->email->Email = $request->get('email');
                $customer->phoneNumber->PhoneNumber = $request->get('phone_number');

                $customer->push();
            }
        }
        return redirect('search');
    }

    function log(Request $request)
    {
        $ticket = new Ticket($request->all());
        $ticket->Id = Employee::where('user_id', Auth::user()->id)->pluck('ID')->first();
        $ticket->save();
        return redirect('home');
    }



    public function getTickets($employeeId)
    {
        $tickets = Ticket::where('ID', $employeeId)->get();

        return view('employee.employee')->with(compact('tickets','employeeId'));
    }


    public function getEmployeeTicket($employeeId, $startDate = null, $endDate = null)
    {
        if ($startDate == null || $endDate == null){
            $startDate = Carbon::create(2000,01,01);
            $endDate = Carbon::now()->toDateString();
            $tickets = Ticket::whereBetween('Date_of_Call', array($startDate, $endDate))->where('ID',$employeeId)->get();
        }else {
            $tickets = Ticket::whereBetween('Date_of_Call', array($startDate, $endDate))->where('ID', $employeeId)->get();
        }
        return $tickets;
    }

}
