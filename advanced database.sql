CREATE TABLE migrations
(
  id        INT IDENTITY
    PRIMARY KEY,
  migration NVARCHAR(255) NOT NULL,
  batch     INT           NOT NULL
)
GO

CREATE TABLE Department
(
  Depart_id       INT IDENTITY (100, 1)
    PRIMARY KEY,
  Description     VARCHAR(50) NOT NULL,
  Department_Name VARCHAR(50) NOT NULL
)
GO

CREATE TABLE AccountHolders
(
  ID              INT IDENTITY (100, 1)
    PRIMARY KEY,
  First_Name      VARCHAR(50)   NOT NULL,
  Last_Name       VARCHAR(50)   NOT NULL,
  Gender          CHAR(1)
    CHECK ([GENDER] = 'F' OR [GENDER] = 'M'),
  DOB             DATE          NOT NULL,
  Address         VARCHAR(2000) NOT NULL,
  Salary          MONEY,
  Customer_rating VARCHAR(50),
  Supervisor_id   INT,
  Depart_ID       INT
    CONSTRAINT FK_DEPART_ID
    REFERENCES Department,
  Age             INT,
  user_id         INT
)
GO

CREATE TABLE users
(
  id             INT IDENTITY
    PRIMARY KEY,
  name           NVARCHAR(255) NOT NULL,
  email          NVARCHAR(255) NOT NULL,
  password       NVARCHAR(255) NOT NULL,
  remember_token NVARCHAR(100),
  created_at     DATETIME,
  updated_at     DATETIME
)
GO

CREATE UNIQUE INDEX users_email_unique
  ON users (email)
GO

ALTER TABLE AccountHolders
  ADD CONSTRAINT AccountHolders_users_id_fk
FOREIGN KEY (user_id) REFERENCES users
GO

CREATE TABLE Ticket
(
  Ticket_Number INT IDENTITY (100, 1)
    PRIMARY KEY,
  Date_of_call  DATE        NOT NULL,
  category      VARCHAR(50) NOT NULL
    CHECK ([CATEGORY] = 'Payment Verification' OR [CATEGORY] = 'Balance Query'),
  ID            INT
    CONSTRAINT fk_ticket
    REFERENCES AccountHolders
)
GO

CREATE TRIGGER create_call
  ON Ticket
  AFTER INSERT AS
  BEGIN
    INSERT INTO Call (Ticket#, Id_num)
      SELECT
        inserted.Ticket_Number,
        inserted.ID
      FROM inserted;
  END
GO

CREATE TABLE password_resets
(
  email      NVARCHAR(255) NOT NULL,
  token      NVARCHAR(255) NOT NULL,
  created_at DATETIME
)
GO

CREATE INDEX password_resets_email_index
  ON password_resets (email)
GO

CREATE TABLE roles
(
  id          INT IDENTITY
    PRIMARY KEY,
  name        NVARCHAR(255) NOT NULL,
  description NVARCHAR(255) NOT NULL,
  created_at  DATETIME,
  updated_at  DATETIME
)
GO

CREATE TABLE role_user
(
  id         INT IDENTITY
    PRIMARY KEY,
  role_id    INT NOT NULL,
  user_id    INT NOT NULL,
  created_at DATETIME,
  updated_at DATETIME
)
GO

CREATE TABLE CreditCard
(
  CardNum  VARCHAR(16) NOT NULL,
  CardType VARCHAR(50) NOT NULL
    CHECK ([CardType] = 'Current' OR [CardType] = 'Savings'),
  ID       INT
    CONSTRAINT FK_CARD
    REFERENCES AccountHolders,
  CardId   INT IDENTITY
    CONSTRAINT CreditCard_CardId_pk
    PRIMARY KEY
)
GO

CREATE UNIQUE INDEX CreditCard_CardNum_uindex
  ON CreditCard (CardNum)
GO

CREATE TABLE Call
(
  Call_Number INT IDENTITY (10, 1)
    PRIMARY KEY,
  [Ticket#]   INT
    CONSTRAINT FK_CALL
    REFERENCES Ticket,
  Id_num      INT
    CONSTRAINT fk_num
    REFERENCES AccountHolders
)
GO

CREATE TABLE Transact
(
  TransactionNum INT         NOT NULL
    PRIMARY KEY,
  Location       VARCHAR(50) NOT NULL,
  Date           DATE        NOT NULL,
  Card_num       INT
    CONSTRAINT Transact_CreditCard_CardId_fk
    REFERENCES CreditCard
)
GO

CREATE TABLE Account
(
  AccountNum   VARCHAR(10) NOT NULL,
  Account_Type VARCHAR(20) NOT NULL,
  ID           INT
    CONSTRAINT fk_number
    REFERENCES AccountHolders,
  AccountId    INT IDENTITY
    CONSTRAINT Account_AccountId_pk
    PRIMARY KEY
)
GO

CREATE UNIQUE INDEX Account_AccountNum_uindex
  ON Account (AccountNum)
GO

CREATE TABLE Payments
(
  PaymentNo      INT IDENTITY
    PRIMARY KEY,
  CardNo         INT
    CONSTRAINT Payments_CreditCard_CardId_fk
    REFERENCES CreditCard,
  payment_amount MONEY,
  Date           DATETIME NOT NULL
)
GO

CREATE TABLE AccountHolders_Email
(
  account_holder_id INT         NOT NULL
    CONSTRAINT FK_ID
    REFERENCES AccountHolders,
  Email             VARCHAR(50) NOT NULL,
  CONSTRAINT EmailUnique
  UNIQUE (account_holder_id, Email)
)
GO

CREATE TABLE AccountHolders_PhoneNumber
(
  account_holder_id INT         NOT NULL
    CONSTRAINT FK_phone
    REFERENCES AccountHolders,
  PhoneNumber       VARCHAR(50) NOT NULL,
  CONSTRAINT PhoneNumberUnique
  UNIQUE (account_holder_id, PhoneNumber)
)
GO

CREATE VIEW Employees AS
  SELECT
    ID,
    First_Name,
    Last_Name,
    Gender,
    DOB,
    Address,
    Age,
    Salary,
    Depart_ID,
    Supervisor_id,
    user_id
  FROM AccountHolders
  WHERE Salary IS NOT NULL
GO

CREATE VIEW Customers AS
  SELECT
    ID,
    First_Name,
    Last_Name,
    Gender,
    Address,
    Age,
    Customer_rating,
    user_id
  FROM AccountHolders
  WHERE Salary IS NULL
GO

CREATE VIEW Calls AS
  SELECT
    First_Name,
    Last_Name
  FROM Customers
    INNER JOIN Call ON Call.Id_num = Customers.ID
GO

CREATE VIEW CustomerEmployees AS
  SELECT
    First_Name,
    Last_Name,
    Gender,
    DOB,
    Address,
    Age,
    Salary,
    Customer_rating
  FROM AccountHolders
  WHERE Salary IS NOT NULL AND Customer_rating IS NOT NULL
GO

CREATE VIEW Tickets AS
  SELECT
    First_Name,
    Last_Name,
    Date_of_call,
    category
  FROM Ticket
    INNER JOIN Customers ON Customers.ID = Ticket.ID
GO

CREATE VIEW CreditCards AS
  SELECT
    First_Name,
    Last_Name,
    CardNum
  FROM Customers
    INNER JOIN CreditCard ON Customers.ID = CreditCard.ID
GO

CREATE VIEW Accounts AS
  SELECT
    First_Name,
    Last_Name,
    Account_Type
  FROM Customers
    INNER JOIN Account ON Customers.ID = Account.ID
GO

CREATE VIEW Transactions AS
  SELECT
    First_Name,
    Last_Name,
    Location,
    Date,
    Card_num
  FROM Transact
    INNER JOIN CreditCard ON Transact.Card_num = CreditCard.CardNum
    INNER JOIN Customers ON CreditCard.ID = Customers.ID
GO

CREATE FUNCTION getTotalByDepartment()
  RETURNS TABLE
  AS
  RETURN SELECT
           Department_Name,
           count(*) AS Total
         FROM Department
         GROUP BY Department_Name
GO

CREATE PROC sp_insert_customer(
  @firstName VARCHAR(50),
  @lastName  VARCHAR(50),
  @Gender    CHAR(1),
  @Dob       DATE,
  @Address   VARCHAR(2000)
)
AS
  BEGIN TRANSACTION
  INSERT INTO AccountHolders (First_Name, Last_Name, Gender, DOB, Address)
  VALUES (@firstName, @lastName, @Gender, @Dob, @Address)
  IF (DATEDIFF(YY, @Dob, GETDATE()) < 20)
    BEGIN
      ROLLBACK TRANSACTION
    END
  COMMIT TRANSACTION
GO

CREATE PROC sp_view_transactions_between_dates
  (@start_date AS DATE,
   @end_date AS   DATE)
AS
  BEGIN
    SELECT
      AccountHolders.First_Name,
      AccountHolders.Last_Name,
      AccountHolders.DOB,
      CreditCard.CardNum,
      Transact.Date,
      Transact.Location
    FROM AccountHolders
      INNER JOIN CreditCard ON AccountHolders.ID = CreditCard.ID
      INNER JOIN Transact ON Transact.Card_num = CreditCard.CardNum
    WHERE Date BETWEEN @start_date AND @end_date;
  END
GO

CREATE FUNCTION fn_calculate_employee_tax(@tax_type VARCHAR(10), @salary SMALLMONEY)
  RETURNS VARCHAR(20)
AS
  BEGIN
    DECLARE @nis_tax SMALLMONEY
    DECLARE @statutory_income SMALLMONEY;
    DECLARE @tax_amount VARCHAR(20);
    -- Calculating the NIS
    SET @nis_tax = @salary * .025;
    -- Set the threshhold of the NIS if greater than $37,500.00
    IF (@nis_tax > 37500)
      SET @nis_tax = 37500;

    SET @statutory_income = @salary - @nis_tax;

    IF @tax_type = 'NHT'  -- Calculating the NHT Tax
      BEGIN
        SET @tax_amount = @salary * .02;
      END
    ELSE IF (@tax_type = 'NIS')  -- Calculating the NIS Tax
      BEGIN
        SET @tax_amount = @nis_tax;
      END
    ELSE IF (@tax_type = 'Edu')  -- Calculating the Education Tax
      BEGIN
        SET @tax_amount = @statutory_income * .0225;
      END
    ELSE IF (@tax_type = 'Paye')-- Calculating the Pay as you earn Tax
      BEGIN
        SET @tax_amount = @statutory_income * .25;
      END
    ELSE
      SET @tax_amount = 'Invalid Tax Type';

    RETURN @tax_amount;
  END
GO

CREATE FUNCTION fn_view_supervisors_fifty_and_over()
  RETURNS TABLE
  AS
  RETURN SELECT
           First_Name,
           Last_Name,
           DOB,
           Salary,
           DATEDIFF(YEAR, DOB, GETDATE()) AS 'Age'
         FROM
           AccountHolders
         WHERE Supervisor_id IS NOT NULL AND DATEDIFF(YEAR, DOB, GETDATE()) >= 50
GO

CREATE FUNCTION fn_view_number_of_tickets_by_employee_over_given_dates(@start_date DATE, @end_date DATE)
  RETURNS TABLE
  AS
  RETURN SELECT
           AccountHolders.First_Name,
           AccountHolders.Last_Name,
           (SELECT COUNT(category)
            FROM Ticket
            WHERE Ticket.ID = AccountHolders.ID AND Ticket.category = 'Balance Query' AND
                  Ticket.Date_of_call BETWEEN @start_date AND @end_date) AS 'Balance Query',
           (SELECT COUNT(category)
            FROM Ticket
            WHERE Ticket.ID = AccountHolders.ID AND Ticket.category = 'Payment Verification' AND
                  Ticket.Date_of_call BETWEEN @start_date AND @end_date) AS 'Payment Verification'
         FROM AccountHolders
GO

CREATE PROC sp_update_customer_payment_info(@card_num         INT, @amount_charged MONEY, @location VARCHAR(200),
                                            @transaction_date DATE)
AS
  BEGIN
    UPDATE Payments
    SET payment_amount = @amount_charged, CardNo = @card_num
    BEGIN TRANSACTION
    IF (@@ROWCOUNT > 0)
      BEGIN
        UPDATE Transactions
        SET Location = @location, Date = @transaction_date, Card_num = @card_num
        COMMIT TRANSACTION
      END
    ELSE
      BEGIN
        ROLLBACK
      END
  END
GO

CREATE PROC sp_get_employees_in_department(@departmentName VARCHAR(50))
AS
  BEGIN
    SELECT
      ID,
      First_Name,
      Last_Name,
      Gender,
      Department_Name
    FROM Employees
      INNER JOIN Department ON Department.Depart_id = Employees.Depart_ID
    WHERE Department_Name = @departmentName AND Supervisor_id IS NOT NULL
  END
GO

CREATE PROC sp_count_all_customers
AS
  BEGIN
    SELECT count(*)
    FROM Customers;
  END
GO

CREATE PROC sp_count_all_employee
AS
  BEGIN
    SELECT count(*)
    FROM Employees;
  END
GO

CREATE PROC sp_get_all_transactions_by_card_num(@card_num INT)
AS
  BEGIN
    SELECT
      First_Name,
      Last_Name,
      Location,
      Date,
      Card_num
    FROM Transact
      INNER JOIN CreditCard ON Transact.Card_num = CreditCard.CardNum
      INNER JOIN Customers ON CreditCard.ID = Customers.ID
    WHERE CreditCard.CardNum = @card_num
  END
GO


