<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title"> {{Auth::user()->name}}</li>

            <li class="nav-item">
                <a href="{{route('home')}}" class="nav-link active">
                    <i class="icon icon-speedometer"></i>Dashboard
                </a>
            </li>

            @if(Auth::user()->hasRole('administrator'))
                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin::employees')}}">
                        <i class="icon icon-people" aria-hidden="false"></i>
                        <span class="menu-title">Manage/View Employees</span>
                    </a>
                </li>
            @elseif(Auth::user()->hasRole('employee'))
                <li class="nav-item">
                    <a class="nav-link" href="{{route('employee::search')}}">
                        <i class="icon icon-magnifier" aria-hidden="false"></i>
                        <span class="menu-title">Search/Create Customer</span>
                    </a>
                </li>

            @elseif(Auth::user()->hasRole('supervisor'))
                <li class="nav-item">
                    <a class="nav-link" href="{{route('employee::employeeslist')}}">
                        <i class="icon icon-people" aria-hidden="false"></i>
                        <span class="menu-title">View Employees</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('employee::search')}}">
                        <i class="icon icon-magnifier" aria-hidden="false"></i>
                        <span class="menu-title">Search/Create Customer</span>
                    </a>
                </li>
            @elseif(Auth::user()->hasRole('customer'))

            @endif
        </ul>
    </nav>
</div>
