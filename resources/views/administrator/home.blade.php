@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card p-4">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div>
                        <span class="h4 d-block font-weight-normal mb-2">Total Calls</span>
                        <span class="font-weight-light">{{$dashboard[0]}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card p-4">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div>
                        <span class="h4 d-block font-weight-normal mb-2">Total Transactions</span>
                        <span class="font-weight-light">{{$dashboard[1]}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card p-4">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div>
                        <span class="h4 d-block font-weight-normal mb-2">Total cash earned</span>
                        <span class="font-weight-light">${{$dashboard[2]}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
