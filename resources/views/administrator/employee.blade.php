@extends('layouts.app')

@section('content')
    <form class="form-horizontal well" action="{{route('admin::saveemployee')}}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="ID" value="{{ $employee->ID or ''}}">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="First_name" class="form-control-label">First Name:</label>
                    <input type="text" name="First_Name" class="form-control" value="{{ $employee->First_Name or '' }}"
                           id="First_name">
                    <div class="help-block with-errors"></div>
                    {!! ($errors->has('First_Name') ? $errors->first('First_Name','<p class="text-danger">:message</p>') :'') !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="Last_Name" class="form-control-label">Last Name:</label>
                    <input type="text" name="Last_Name" class="form-control" value="{{ $employee->Last_Name or ''}}"
                           id="Last_Name">
                    <div class="help-block with-errors"></div>
                    {!! ($errors->has('Last_Name') ? $errors->first('Last_Name','<p class="text-danger">:message</p>') :'') !!}

                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="DOB" class="form-control-label">Date of birth</label>
                    <div class="form-group">
                        <input type="text" name="DOB" id="DOB" data-provide="datepicker-inline"
                               data-date-format="mm/dd/yyyy" class="form-control"
                               value="{{ $employee->DOB or '' }}"><span
                                class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                        <div class="help-block with-errors"></div>
                        {!! ($errors->has('DOB') ? $errors->first('DOB','<p class="text-danger">:message</p>') :'') !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="Gender">Select ranking</label>
                    <select id="Gender" name="Gender" class="form-control">
                        <option name="male" value="M"{!! $employee->Gender == 'M'? 'selected' : '' !!}>Male</option>
                        <option name="female" value="F" {!! $employee->Gender == 'F'? 'selected' : '' !!}>Female
                        </option>
                    </select>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="phone_number" class="form-control-label">Phone number:</label>
                    <input type="text" name="phone_number" class="form-control" id="phone_number"
                           value="{{ $employee->phoneNumber()->first()['PhoneNumber'] or '' }}">
                    <div class="help-block with-errors"></div>
                    {!! ($errors->has('phone_number') ? $errors->first('phone_number','<p class="text-danger">:message</p>') :'') !!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="email" class="form-control-label">Email:</label>
                    <input type="email" name="email" class="form-control" id="email"
                           value="{{ $employee->email()->first()['Email'] or '' }}">
                    <div class="help-block with-errors"></div>
                    {!! ($errors->has('email') ? $errors->first('email','<p class="text-danger">:message</p>') :'') !!}
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="street_name" class="form-control-label">Street Name:</label>
                    <input type="text" name="street_name" class="form-control" id="street_name"
                           value="{{$address[0] or ''}}">
                    <div class="help-block with-errors"></div>
                    {!! ($errors->has('street_name') ? $errors->first('street_name','<p class="text-danger">:message</p>') :'') !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="parish" class="form-control-label">Parish:</label>
                    <input type="text" name="parish" class="form-control" id="parish" value="{{$address[1] or ''}}">
                    <div class="help-block with-errors"></div>
                    {!! ($errors->has('parish') ? $errors->first('parish','<p class="text-danger">:message</p>') :'') !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="role">Select ranking</label>
                    <select id="role" name="role" class="form-control">
                        <option name="supervisor" value="supervisor" {!! $role == 'supervisor'? 'selected' : '' !!}>
                            Supervisor
                        </option>
                        <option name="employee" value="employee" {!! $role == 'employee'? 'selected' : '' !!}>Regular
                            Employee
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="department">Select Department</label>
                    <select id="department" name="Department" class="form-control">
                        @foreach($departments as $department)
                            <option value="{{$department->Depart_id}}"
                                    {!! $department->Depart_id == $employee->Depart_ID ? 'selected' : ''!!}
                                    name="{{$department->Department_Name}}">{{$department->Department_Name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="salary" class="form-control-label">Salary:</label>
                    <input type="number" name="salary" class="form-control" id="salary"
                           value="{{ $employee->Salary or ''}}">
                    <div class="help-block with-errors"></div>
                    {!! ($errors->has('salary') ? $errors->first('salary','<p class="text-danger">:message</p>') :'') !!}
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Save Employee</button>
    </form>

@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            var date = new Date();
            $('#DOB').datepicker({
                startDate: '1900-01-01',
                endDate: date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()
            });

            $('#phone_number').mask("(876)999-9999", {placeholder: " "});


            $('#department').on('change', function () {
                var department = $(this).val();
                var url = 'http://' + window.location.host + '/getsupervisor/' + department;

                jQuery.ajax({
                    method: 'get',
                    url: url,
                    success: function (response) {
                        if (($.trim(response))) {
                            $('#role').empty().append('<option name="employee" value="employee">Employee</option>');
                        } else {
                            $('#role').empty()
                                .append('<option name="supervisor" value="supervisor">Supervisor</option>')
                                .append('<option name="employee" value="employee">Employee</option>');
                        }
                    }
                });
            });
        });

    </script>

@endpush