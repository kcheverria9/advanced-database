@extends('layouts.app')
@section('content')

    <input type="hidden" id="ID" name="ID" value="{{ $employeeId }}">


    <div class="row">
        <div class="col-md-4">
            <label>Start Date</label>
            <div class="input-group mb-3">
                <input type="text" class="form-control" id="start_date" name="start_date" placeholder="StartDate">
            </div>
        </div>

        <div class="col-md-4">
            <label>End Date</label>

            <div class="input-group mb-3">
                <input type="text" class="form-control" name="end_date" id="end_date" placeholder="End Date">
            </div>
        </div>
        <div class="col-md-4">
            <label>&nbsp;</label>
            <div class="input-group mb-3">
                <button  class="btn btn-primary" id="submit">Update</button>
            </div>
        </div>

    </div>

    <div class="card-body" style="margin-top: 3em;">
        <div class="table-responsive">
            <table border=1 class="table">
                <thead>
                <tr>
                    <td>Ticket Number</td>
                    <td>Category</td>
                    <td>Date of call</td>
                </tr>
                </thead>
                <tbody id="tickets">
                @foreach($tickets as $ticket)
                    <tr>
                        <td>{{$ticket->Ticket_Number}}</td>
                        <td>{{$ticket->category}}</td>
                        <td>{{$ticket->Date_of_call}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        $(document).ready(function () {
            var date = new Date();
            $('#start_date').datepicker({
                startDate: '1900-01-01',
                endDate: date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()
            });

            $('#end_date').datepicker({
                startDate: '1900-01-01',
                endDate: date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()
            });

            $('#submit').on('click',function ()  {
                var startDate = $('#start_date').val();
                var endDate = $('#end_date').val();
                var employeeId = $('#ID').val();
                console.log(employeeId);
                var url = 'http://' + window.location.host + '/ticketsbyemployee/' + employeeId + '/' + startDate + '/' + endDate;

                $.ajax({
                    type: 'get',
                    url: url,
                    dataType: 'json',
                    success: function (response) {
                        console.log(response);
                        if(($.trim(response))){
                            $.each(response, function (key, val) {
                                $('#tickets').empty().append('<tr>\n' +
                                    '                        <td>' + val['Ticket_Number'] + '</td>\n' +
                                    '                        <td>' + val['category'] + '</td>\n' +
                                    '                        <td>' + val['Date_of_call'] + '</td>\n' +
                                    '                    </tr>');
                            });
                        }else {
                            $('#tickets').empty().append('<tr>\n' +
                                '                        <td>' + 'n/a' + '</td>\n' +
                                '                        <td>' + 'n/a' + '</td>\n' +
                                '                        <td>' + 'n/a' + '</td>\n' +
                                '                    </tr>');
                        }
                    },
                    error: function (status) {
                        console.log(status);
                    }

                });
            });


        });
    </script>

@endpush