@extends('layouts.app')

@section('content')

    <div class="row pull-right">
        <a data-toggle="modal" data-target="#ticket-modal" aria-hidden="false" class="btn btn-secondary">
            <i class="icon icon-wallet"></i>
            <span class="d-md-inline-block">Make a ticket</span>
        </a>
    </div>

    <div class="modal" id="ticket-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create Ticket</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="call" action="{{route('employee::log')}}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="category">Select Category</label>
                            <select id="category" name="category" class="form-control">
                                <option id="payment" value="Payment Verification">Payment Verification</option>
                                <option id="balance" value="Balance Query">Balance Query</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input name="Date_of_call" id="Date_of_call" type="text" placeholder="Date of call"
                                   class="form-control">
                        </div>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="card-body" style="margin-top: 3em;">
        <div class="table-responsive">
            <table border=1 class="table">
                <thead>
                <tr>
                    <td>Ticket Number</td>
                    <td>Category</td>
                    <td>Date of call</td>
                </tr>
                </thead>
                @foreach($tickets as $ticket)
                    <tr>
                        <td>{{$ticket->Ticket_Number}}</td>
                        <td>{{$ticket->category}}</td>
                        <td>{{$ticket->Date_of_call}}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {

            var date = new Date();
            $('#Date_of_call').datepicker({
                startDate: date.getFullYear()+"-"+(date.getMonth()+1)+"-"+(date.getDate()-1),
                endDate: date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()
            });
        });
    </script>
@endpush
