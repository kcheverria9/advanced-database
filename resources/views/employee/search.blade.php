@extends('layouts.app')

@section('content')
    <div class="row" style="margin-bottom: 3em;">
        <div class="col-md-10">
            <div class="input-group">
                <input type="email" id="search" name="input1-group2" class="form-control" placeholder="Email">
            </div>
        </div>
        <div class="col-md-2 pull-right">
            <a href="{{route('employee::savecustomer')}}" class="btn btn-primary">New Customer</a>
        </div>
    </div>

    <div id="customers" class="row">
        @foreach($customers as $customer)
            <div class="col-md-4">
                <div class="card p-4">
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <div>
                            <a href="{{route('employee::savecustomer',$customer->ID)}}"><span class="h4 d-block font-weight-normal mb-2">{{$customer->name}}</span></a>
                        </div>
                        @if($customer->Gender == "F")
                            <div class="h2 text-muted">
                                <i class="icon icon-user-female"></i>
                            </div>
                        @elseif($customer->Gender == "M")
                            <div class="h2 text-muted">
                                <i class="icon icon-user"></i>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#search').on('change keyup paste', function () {
                var search = $(this).val();
                var url = 'http://' + window.location.host + '/getcustomer/' + search;
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: url,
                    success: function (data) {
                        $.each(data, function (key, val) {
                            var address = 'http://' + window.location.host + '/customer/' + val['ID'];
                            $('#customers').empty().append(
                                '<div class="col-md-4">' +
                                '<div class="card p-4">' +
                                '<div class="card-body d-flex justify-content-between align-items-center">' +
                                '<div>' +
                                '<a href=' + address + '>' + '<span class="h4 d-block font-weight-normal mb-2">' + val['First_Name'] + ' ' + val['Last_Name'] + '</span></a>' +
                                '</div>' +
                                (val['Gender'] === "'F'" ? +'<div class="h2 text-muted">\n' +
                                    '                        <i class="icon icon-user-female"></i>\n' +
                                    '                    </div>' : '<div class="h2 text-muted">\n' +
                                    '                        <i class="icon icon-user"></i>\n' +
                                    '                    </div>') +
                                '</div>' +
                                '</div>' +
                                '</div>'
                            )
                            ;
                        });
                    }
                });
            });
        });
    </script>
@endpush