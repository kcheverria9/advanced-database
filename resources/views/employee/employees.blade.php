@extends('layouts.app')

@section('content')

    <div class="row">
        @foreach($employeeArray as $employee)
            <div class="col-md-4">
                <div class="card p-4">
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <div>
                            <a href="{{route('employee::tickets',$employee->ID)}}"><span class="h4 d-block font-weight-normal mb-2">{{$employee->name}}</span></a>
                            <span class="font-weight-light">{{$employee->Department_Name}}</span>
                        </div>
                        @if($employee->Gender == "F")
                            <div class="h2 text-muted">
                                <i class="icon icon-user-female"></i>
                            </div>
                        @elseif($employee->Gender == "M")
                            <div class="h2 text-muted">
                                <i class="icon icon-user"></i>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection

