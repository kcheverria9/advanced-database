@extends('layouts.app')

@section('content')

    <form class="form-horizontal well" action="{{route('employee::save')}}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="ID" value="{{ $customer->ID or ''}}">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="First_name" class="form-control-label">First Name:</label>
                    <input type="text" name="First_Name" class="form-control" value="{{ $customer->First_Name or '' }}"
                           id="First_name">
                    <div class="help-block with-errors"></div>
                    {!! ($errors->has('First_Name') ? $errors->first('First_Name','<p class="text-danger">:message</p>') :'') !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="Last_Name" class="form-control-label">Last Name:</label>
                    <input type="text" name="Last_Name" class="form-control" value="{{ $customer->Last_Name or ''}}"
                           id="Last_Name">
                    <div class="help-block with-errors"></div>
                    {!! ($errors->has('Last_Name') ? $errors->first('Last_Name','<p class="text-danger">:message</p>') :'') !!}

                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="DOB" class="form-control-label">Date of birth</label>
                    <div class="form-group">
                        <input type="text" name="DOB" id="DOB" data-provide="datepicker-inline"
                               data-date-format="mm/dd/yyyy" class="form-control"
                               value="{{ $customer->DOB or '' }}"><span
                                class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                        <div class="help-block with-errors"></div>
                        {!! ($errors->has('DOB') ? $errors->first('DOB','<p class="text-danger">:message</p>') :'') !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="Gender">Gender</label>
                    <select id="Gender" name="Gender" class="form-control">
                        <option name="male" value="M"{!! $customer->Gender == 'M'? 'selected' : '' !!}>Male</option>
                        <option name="female" value="F" {!! $customer->Gender == 'F'? 'selected' : '' !!}>Female
                        </option>
                    </select>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="phone_number" class="form-control-label">Phone number:</label>
                    <input type="text" name="phone_number" class="form-control" id="phone_number"
                           value="{{ $customer->phoneNumber()->first()['PhoneNumber'] or '' }}">
                    <div class="help-block with-errors"></div>
                    {!! ($errors->has('phone_number') ? $errors->first('phone_number','<p class="text-danger">:message</p>') :'') !!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="email" class="form-control-label">Email:</label>
                    <input type="email" name="email" class="form-control" id="email"
                           value="{{ $customer->email()->first()['Email'] or '' }}">
                    <div class="help-block with-errors"></div>
                    {!! ($errors->has('email') ? $errors->first('email','<p class="text-danger">:message</p>') :'') !!}
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="street_name" class="form-control-label">Street Name:</label>
                    <input type="text" name="street_name" class="form-control" id="street_name"
                           value="{{$address[0] or ''}}">
                    <div class="help-block with-errors"></div>
                    {!! ($errors->has('street_name') ? $errors->first('street_name','<p class="text-danger">:message</p>') :'') !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="parish" class="form-control-label">Parish:</label>
                    <input type="text" name="parish" class="form-control" id="parish" value="{{$address[1] or ''}}">
                    <div class="help-block with-errors"></div>
                    {!! ($errors->has('parish') ? $errors->first('parish','<p class="text-danger">:message</p>') :'') !!}
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="card_number" class="form-control-label">Card Number</label>
                    <input type="text" name="card_number" class="form-control" id="card_number"
                           value="{{$card->CardNum or ''}}">
                    <div class="help-block with-errors"></div>
                    {!! ($errors->has('card_number') ? $errors->first('card_number','<p class="text-danger">:message</p>') :'') !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="CardType">Select CardType</label>
                    <select id="CardType" name="CardType" class="form-control">
                        <option name="current" value="Current"{!! $card->CardType == 'Current'? 'selected' : '' !!}>Current</option>
                        <option name="savings" value="Savings" {!! $card->CardType == 'Savings'? 'selected' : '' !!}>Savings</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="account_number" class="form-control-label">Account Number</label>
                    <input type="text" name="account_number" class="form-control" id="account_number"
                           value="{{$account->AccountNum or ''}}">
                    <div class="help-block with-errors"></div>
                    {!! ($errors->has('account_number') ? $errors->first('account_number','<p class="text-danger">:message</p>') :'') !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="AccountType">Select AccountType</label>
                    <select id="AccountType" name="AccountType" class="form-control">
                        <option name="Active" value="Active"{!! $account->Account_Type == 'Active' ? 'selected' : '' !!}>Active</option>
                        <option name="Dormant" value="Dormant" {!! $account->Account_Type == 'Dormant' ? 'selected' : '' !!}>Dormant</option>
                        <option name="Seldom" value="Seldom" {!! $account->Account_Type == 'Seldom' ? 'selected' : '' !!}>Seldom</option>
                    </select>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Save Customer</button>
    </form>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            var date = new Date();
            $('#DOB').datepicker({
                startDate: '1900-01-01',
                endDate: date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()
            });

            $('#phone_number').mask("(876)999-9999", {placeholder: " "});
            $('#card_number').mask("999999-9-999999999", {placeholder: " "});
            $('#account_number').mask("999-999-999", {placeholder: " "});
        });
    </script>

@endpush