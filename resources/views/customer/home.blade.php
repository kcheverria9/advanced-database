@extends('layouts.app')

@section('content')
    <div class="row pull-right">
        <a data-toggle="modal" data-target="#payment-modal" aria-hidden="false" class="btn btn-secondary">
            <i class="icon icon-wallet"></i>
            <span class="d-md-inline-block">Make a payment</span>

        </a>
    </div>

    <div class="modal" id="payment-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Make Payment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="payment" action="{{route('customer::payment')}}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="CardNo">Select Card</label>
                            <select id="CardNo" name="CardNo" class="form-control">
                                @foreach($payments as $card)
                                    <option id="{{$card->CardId}}" value="{{$card->CardId}}">{{$card->CardNum}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input name="payment_amount" type="text" placeholder="Amount to be paid" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div class="card-body" style="margin-top: 3em;">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Payment Number</th>
                    <th>Card Number</th>
                    <th>Payment Amount</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($payments as $payment)
                    @foreach($payment->payment as $item)
                        <tr>
                            <td>{{$item->PaymentNo}}</td>
                            <td class="text-nowrap">{{$payment->CardNum}}</td>
                            <td>{{$item->payment_amount}}</td>
                            <td>{{$item->Date}}</td>
                        </tr>
                    @endforeach

                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

