<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\AccountHolder;
use App\Models\AccountHolderEmail;
use App\Models\CreditCard;
use App\Models\Customer;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::get('/home', ['uses' => 'HomeController@index'])->name('home');

    Route::group(['namespace' => 'Admin', 'as' => 'admin::'], function () {
        Route::get('employees', ['uses' => 'AdminController@employee'])->name('employees');

        Route::get('employees/form/{id?}', ['uses'=>'AdminController@form'])->name('new');

        Route::post('employees/save', ['uses' => 'AdminController@saveEmployee'])->name('saveemployee');

        Route::get('/getsupervisor/{id}', function ($id) {
            $departmentSuperVisor = AccountHolder::where('Supervisor_id', null)->where('Depart_ID', $id)->get();
            return $departmentSuperVisor;
        });
    });
    Route::group(['namespace' => 'Employee', 'as' => 'employee::'], function () { 
        Route::get('employeeslist',['uses' => 'EmployeeController@employees'])->name('employeeslist');

        Route::get('search',function (){
            $customers = Customer::all();
            return view('employee.search')->with(compact('customers'));
        })->name('search');

        Route::get('/getcustomer/{email?}',function ($email){
            $customers = AccountHolderEmail::where('Email',$email)->first()->customer;
            if ($customers){
                return response()->json(['customer'=>$customers],200);
            }
            return response()->json(['msg'=> 'Nothing found'],404);
        });

        Route::get('/customer/form/{id?}',['uses'=> 'EmployeeController@form'])->name('savecustomer');

        Route::post('/customer/save',['uses'=> 'EmployeeController@saveCustomer'])->name('save');


        Route::post('/log',['uses' => 'EmployeeController@log'])->name('log');

        Route::get('/tickets/{employeeId}', ['uses' => 'EmployeeController@getTickets'])->name('tickets');

        Route::get('/ticketsbyemployee/{empId}/{startDate?}/{endDate?}', ['uses'=>'EmployeeController@getEmployeeTicket'])->name('employeeTicket');
    });

    Route::group(['namespace'=> 'Customer', 'as' => 'customer::'], function (){
        Route::post('/payment',['uses' => 'CustomerController@payment'])->name('payment');
    });

});




