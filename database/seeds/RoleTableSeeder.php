<?php

use Illuminate\Database\Seeder;
use \App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $supervisor = new Role();
        $supervisor->name = 'supervisor';
        $supervisor->description = 'supervisor';
        $supervisor->save();

        $employee = new Role();
        $employee->name = 'employee';
        $employee->description = 'employee';
        $employee->save();

        $admin = new Role();
        $admin->name = 'administrator';
        $admin->description= 'administrator';
        $admin->save();

        $customer = new Role();
        $customer->name = 'customer';
        $customer->description= 'customer';
        $customer->save();
    }
}
